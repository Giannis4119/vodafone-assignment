import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch, Link, Redirect } from "react-router-dom";
import Home from './Home/Home';
import Page2 from './Page2/Page2';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

function App() {

  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  return (
    <div className="App">
      
      <Router>
        <header className="App-header">
          <nav>
            <div className="row">
              <ul className="main-nav">
                <li><Link to="/home">Home</Link></li>
                <li><Link to="/page2">Page 2</Link></li>
              </ul>
            </div>
          </nav>
        </header>
          <Slider {...settings}>
            <div>
              <h3>Slide 1</h3>
              <p>Text for slide 1</p>
            </div>
            <div>
              <h3>Slide 2</h3>
              <p>Text for slide 2</p>
            </div>
            <div>
              <h3>Slide 3</h3>
              <p>Text for slide 3</p>
            </div>
          </Slider>
          
          <div className="row">

            <Switch>
                <Route path="/home" exact component={Home} />
                <Route path="/page2" exact component={Page2} />       
                <Redirect to={"/home"} />      
            </Switch>
          </div>
      </Router>        
    </div>
  );
}

export default App;
