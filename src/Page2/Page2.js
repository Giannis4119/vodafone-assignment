import React, { useEffect } from 'react'
import { getPage2Content } from '../action_creators/app_action_creators'
import { connect } from 'react-redux'
import './Page2.css'

const Page2 = (props) => {

  const { page2Content } = props

  useEffect(() => {
    props.getPage2Content()
  },[])

  useEffect(() => {
    console.log(page2Content)
  },[page2Content])

  const { tiles } = page2Content

  return (
    <div>
      <h1>{page2Content && page2Content.description}</h1>
      <div className="page2-content">
        {
          (tiles && Boolean(tiles.length)) && 
          tiles.map((tile, index) => (
            <div key={index} class="tile-content">
              <h5>{tile.title}</h5>
              <p>{tile.description}</p>
              <p><button>{tile.link}</button></p>
            </div>
          ))
        }
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  page2Content: state.page2Content
})

const mapDispatchToProps = {
  getPage2Content
}

export default connect(mapStateToProps, mapDispatchToProps)(Page2);