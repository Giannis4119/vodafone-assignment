import axios from 'axios'

export const getHomeContent = () => (
  dispatch => {
    axios.get('https://voda-react-assessment.herokuapp.com/home')
    .then(response => {
      dispatch(
          {
            type: 'FETCH_HOME_CONTENT',
            payload: {
              homeContent: response.data.shift()
            }
          }
        )
    })
  }
)

export const getPage2Content = () => (
  dispatch => {
    axios.get('https://voda-react-assessment.herokuapp.com/page')
    .then(response => {
      dispatch(
          {
            type: 'FETCH_PAGE2_CONTENT',
            payload: {
              page2Content: response.data.shift()
            }
          }
        )
    })
  }
)

export const getMenuContent = () => (
  dispatch => {
    axios.get('https://voda-react-assessment.herokuapp.com/menu')
    .then(response => {
      dispatch(
          {
            type: 'FETCH_MENU_CONTENT',
            payload: {
              page2Content: response.data
            }
          }
        )
    })
  }
)

export const getSliderContent = () => (
  dispatch => {
    axios.get('https://voda-react-assessment.herokuapp.com/slider')
    .then(response => {
      dispatch(
          {
            type: 'FETCH_SLIDER_CONTENT',
            payload: {
              page2Content: response.data
            }
          }
        )
    })
  }
)