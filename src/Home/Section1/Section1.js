import React from 'react'
import './Section1.css'
const Section1 = (props) => {
  
  const { images } = props

  return (
    <div id="gallery">
      {
        (images && Boolean(images.length)) && 
        images.map((image, index) => (
          <img key={index} className={`gallery__img gallery__item--${index+1}`} src={image.img} title={image.title}></img>
        ))
      }
    </div>
  )
}

export default Section1