import React, { Fragment } from 'react'
import './Section2.css'
const Section2 = (props) => {
 
  const { stats } = props
  const validateUserData = (e) => {
    e.preventDefault()
    alert('Form data validated successfully')
  }

  return (
    <Fragment>     
      <div>        
        <div className="section2-content">
          <div className="stats">
            <h4>{props.graphText}</h4>
            {
              (stats && Boolean(stats.length)) && 
              stats.map((stat, index) => (
                <Fragment key={index}>
                  <div className="stat-title">
                    <span>{stat.title}</span>
                    <span>{stat.amount ? stat.amount / 10 : 0}%</span>
                  </div>
                  <progress className={`progress--${index+1}`} max="1000" value={stat.amount}></progress>
                </Fragment>
              ))
            }
          </div>
          <div className="form-data">
            <h4>{props.formText}</h4>
            <small>we work with ecosystem leaders, corporations and startups worldwide. How can we help you?</small>
              <form className="form1" onSubmit={validateUserData}>
                <input placeholder="Your Phone" type="text" minlength="10" pattern="^[2|6]\d{9}$" title="Please define a valid phone. 10 digits starting with 2 or 6." required/>
                <input placeholder="Your Email" type="email" required/>
                <input type="password" placeholder="Password" minlength="8" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" title="Please include at least 1 uppercase character, 1 lowercase character, and 1 number." required/>

                <button>Submit</button>
            </form>
          </div>
        </div>        
      </div>
    </Fragment>
  )
}

export default Section2