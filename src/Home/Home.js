import React, { useState, useEffect, Suspense } from 'react'
import './Home.css'
import Section1 from './Section1/Section1'
import { connect } from 'react-redux'
import { getHomeContent } from '../action_creators/app_action_creators'

const Section2 = React.lazy(() => import('./Section2/Section2'));

const Home = (props) => {

  const { homeContent } = props

  const [displaySection, setDisplaySection] = useState(1)
  const [section1Props, setSection1Props] = useState(1)
  const [section2Props, setSection2Props] = useState(1)


  useEffect(() => {
    props.getHomeContent()
  },[])

  useEffect(() => {
    Boolean(Object.keys(homeContent).length) && setSection1Props(homeContent.sections[0])
    Boolean(Object.keys(homeContent).length) && setSection2Props(homeContent.sections[1])
  },[homeContent])

  return (
    <div>
      <h1>{homeContent && homeContent.description}</h1>
      <header className="home-header">
          <nav>
            <div className="row">
              <ul className="home-nav">
                <li onClick={() => setDisplaySection(1)}>Section 1</li>
                <li onClick={() => setDisplaySection(2)}>Section 2</li>
              </ul>
            </div>
          </nav>
        </header>
        <div className="home-content">
          {
            displaySection === 1 ? <Section1 {...section1Props}/> : (
              <Suspense fallback={<div>Loading...</div>}>
                <Section2 {...section2Props} />
              </Suspense>
            )
          }          
        </div>
    </div>
  )
}

const mapStateToProps = state => ({
  homeContent: state.homeContent
})

const mapDispatchToProps = {
  getHomeContent
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);