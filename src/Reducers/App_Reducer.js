const initState = {
  homeContent: {},
  page2Content: {},
  menuContent: {},
  sliderContent: {}
}

export default (state = initState, action) => {
  switch (action.type) {
    case 'FETCH_HOME_CONTENT':
    return {...state, homeContent: action.payload.homeContent} 

    case 'FETCH_PAGE2_CONTENT': 
    return {...state, page2Content: action.payload.page2Content}

    case 'FETCH_MENU_CONTENT':
    return {...state, menuContent: action.payload.menuContent} 

    case 'FETCH_SLIDER_CONTENT':
    return {...state, sliderContent: action.payload.sliderContent} 
    
    default:
    return state
  }
}